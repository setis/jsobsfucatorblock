<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit', '-1');
set_time_limit(5);
ob_start();
$debug = false;
if ($debug) {
    $start = microtime(true);
}

require __DIR__ . '/Wrapper.php';
require __DIR__ . '/jsmin.php';
require __DIR__ . '/CryptUrl.php';
require __DIR__ . '/Redirect.php';
/**
 * 
 * @param string $Found
 * @param string $notFound
 */
function redirect($Found, $notFound) {
    if (!class_exists('Redirect')) {
        include __DIR__ . '/Redirect.php';
    }
    echo '<html><head></head><body><script type="text/javascript" charset="UTF-8">';
//    echo '<script>';
    echo JSMin::minify(Redirect::run($notFound, $Found));
//    echo '</script>';
    echo '</script></body></html>';
}

global $redis;
$redis = new Redis();
$redis->pconnect('127.0.0.1', 6379);
$host = $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/';

redirect("http://{$host}ref.php#found.html", "http://{$host}ref.php#notfound.html");
$redis->close();
if ($debug) {
    ob_clean();
    var_dump(microtime(true) - $start);
}
