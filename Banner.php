<?php

class Banner {

    /**
     *
     * @var Wrapper
     */
    public $var;

    public function methods() {
        return [
            'lang' => [
                function() {
                    return 'return navigator && (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null);';
                },
                function() {
                    return "return (navigator.language || navigator.systemLanguage || navigator.userLanguage || 'ru').substr(0, 2).toLowerCase();";
                },
                function() {
                    $chroot = $this->var->chroot();
                    $list = $chroot->rand(true, false);
                    $i = $chroot->rand(true, false);
                    $value = $chroot->rand(true, false);
                    return "var $list = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'];
        if (navigator) {
            for (var $i in $list) {
                var $value = {$list}[$i];
                if (navigator[$value]) {
                    return navigator[$value];
                }
            }
        }
            return '';";
                },
                function () {
                    $chroot = $this->var->chroot();
                    $lang = $chroot->rand(true, false);
                    return "  var $lang = '';
        if (navigator) {
            if (navigator.language) {
                $lang = navigator.language;
            } else if (navigator.browserLanguage) {
                $lang = navigator.browserLanguage;
            } else if (navigator.systemLanguage) {
                $lang = navigator.systemLanguage;
            } else if (navigator.userLanguage) {
                $lang = navigator.userLanguage;
            }
        }
        return $lang.substr(0, 2);";
                },
            ],
            'browser' => [
                function() {
                    $chroot = $this->var->chroot();
                    $N = $chroot->rand(true, false);
                    $M = $chroot->rand(true, false);
                    $ua = $chroot->rand(true, false);
                    $tem = $chroot->rand(true, false);
                    return "  var $N = navigator.appName, $ua = navigator.userAgent, $tem;
        var $M = $ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
        if ($M && ($tem = $ua.match(/version\/([\.\d]+)/i)) != null) {
    {$M}[2] = {$tem}[1];
        }
        $M = $M ? [{$M}[1], {$M}[2]] : [$N, navigator.appVersion, '-?'];
        return {$M}[0]+'|'+{$M}[1];";
                },
                function() {
                    $chroot = $this->var->chroot();
                    $BrowserDetect = $chroot->rand(true, false);
                    $init = $chroot->rand(true, false);
                    $browser = $chroot->rand(true, false);
                    $searchString = $chroot->rand(true, false);
                    $dataBrowser = $chroot->rand(true, false);
                    $version = $chroot->rand(true, false);
                    $searchVersion = $chroot->rand(true, false);
                    $data = $chroot->rand(true, false);
                    $i = $chroot->rand(true, false);
                    $dataString = $chroot->rand(true, false);
                    $dataProp = $chroot->rand(true, false);
                    $versionSearchString = $chroot->rand(true, false);
                    $index = $chroot->rand(true, false);
                    $string = $chroot->rand(true, false);
                    $subString = $chroot->rand(true, false);
                    $identity = $chroot->rand(true, false);
                    $versionSearch = $chroot->rand(true, false);
                    $prop = $chroot->rand(true, false);
                    $func = [
                        "$dataBrowser: [
                {
                    $string: navigator.userAgent,
                    $subString: \"Chrome\",
                    $identity: \"Chrome\"
                },
                {
                    $string: navigator.userAgent,
                    $subString: \"OmniWeb\",
                    $versionSearch: \"OmniWeb/\",
                    $identity: \"OmniWeb\"
                },
                {
                    $string: navigator.vendor,
                    $subString: \"Apple\",
                    $identity: \"Safari\",
                    $versionSearch: \"Version\"
                },
                {
                    $prop: window.opera,
                    $identity: \"Opera\"
                },
                {
                    $string: navigator.vendor,
                    $subString: \"iCab\",
                    $identity: \"iCab\"
                },
                {
                    $string: navigator.vendor,
                    $subString: \"KDE\",
                    $identity: \"Konqueror\"
                },
                {
                    $string: navigator.userAgent,
                    $subString: \"Firefox\",
                    $identity: \"Firefox\"
                },
                {
                    $string: navigator.vendor,
                    $subString: \"Camino\",
                    $identity: \"Camino\"
                },
                {
                    $string: navigator.userAgent,
                    $subString: \"Netscape\",
                    $identity: \"Netscape\"
                },
                {
                    $string: navigator.userAgent,
                    $subString: \"MSIE\",
                    $identity: \"Explorer\",
                    $versionSearch: \"MSIE\"
                },
                {
                    $string: navigator.userAgent,
                    $subString: \"Gecko\",
                    $identity: \"Mozilla\",
                    $versionSearch: \"rv\"
                },
                {
                    $string: navigator.userAgent,
                    $subString: \"Mozilla\",
                    $identity: \"Netscape\",
                    $versionSearch: \"Mozilla\"
                }
            ]",
                        "$init: function() {
                this.$browser = this.$searchString(this.$dataBrowser) || \"An unknown browser\";
                this.$version = this.$searchVersion(navigator.userAgent)
                        || this.$searchVersion(navigator.appVersion)
                        || \"an unknown version\";
            }",
                        "$searchString: function($data) {
                for (var $i = 0; $i < $data.length; $i++) {
                    var $dataString = {$data}[$i].$string;
                    var $dataProp = {$data}[$i].$prop;
                    this.$versionSearchString = {$data}[$i].$versionSearch || {$data}[$i].$identity;
                    if ($dataString) {
                        if ($dataString.indexOf({$data}[$i].$subString) != -1)
                            return {$data}[$i].$identity;
                    }
                    else if ($dataProp)
                        return {$data}[$i].$identity;
                }
            }",
                        "$searchVersion: function($dataString) {
                var $index = $dataString.indexOf(this.$versionSearchString);
                if ($index == -1)
                    return;
                return parseFloat($dataString.substring($index + this.$versionSearchString.length + 1));
            }"
                    ];
                    shuffle($func);
                    $result = "var $BrowserDetect = {" . implode(',', $func) . '};' . "$BrowserDetect.$init();";
                    $result.=(rand(0, 1) === 1) ? "return $BrowserDetect.$browser+'|'+$BrowserDetect.$version;" : "return $BrowserDetect.$version+'|'+$BrowserDetect.$browser;";
                    return $result;
                },
                        function() {
                    $chroot = $this->var->chroot();
                    $M = $chroot->rand(true, false);
                    $ua = $chroot->rand(true, false);
                    $tem = $chroot->rand(true, false);
                    return " var $ua = navigator.userAgent, $tem,
        $M = $ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
if (/trident/i.test({$M}[1])) {
    $tem = /\brv[ :]+(\d+)/g.exec($ua) || [];
    return 'IE ' + ({$tem}[1] || '');
}
if ({$M}[1] === 'Chrome') {
    $tem = $ua.match(/\bOPR\/(\d+)/)
    if ($tem != null)
        return 'Opera ' + {$tem}[1];
}
$M = {$M}[2] ? [{$M}[1], {$M}[2]] : [navigator.appName, navigator.appVersion, '-?'];
if (($tem = $ua.match(/version\/(\d+)/i)) != null)
    $M.splice(1, 1, {$tem}[1]);
return {$M}[0]+'|'+{$M}[1];";
                },
                        function() {
                    $chroot = $this->var->chroot();
                    $ua = $chroot->rand(true, false);
                    $check = $chroot->rand(true, false);
                    $r = $chroot->rand(true, false);
                    $DOC = $chroot->rand(true, false);
                    $isStrict = $chroot->rand(true, false);
                    $isOpera = $chroot->rand(true, false);
                    $isChrome = $chroot->rand(true, false);
                    $isWebKit = $chroot->rand(true, false);
                    $isSafari = $chroot->rand(true, false);
                    $isSafari2 = $chroot->rand(true, false);
                    $isSafari3 = $chroot->rand(true, false);
                    $isSafari4 = $chroot->rand(true, false);
                    $isIE = $chroot->rand(true, false);
                    $isIE7 = $chroot->rand(true, false);
                    $isIE8 = $chroot->rand(true, false);
                    $isIE6 = $chroot->rand(true, false);
                    $isGecko = $chroot->rand(true, false);
                    $isGecko2 = $chroot->rand(true, false);
                    $isGecko3 = $chroot->rand(true, false);
                    $isBorderBox = $chroot->rand(true, false);
                    $isStrict = $chroot->rand(true, false);
                    $isWindows = $chroot->rand(true, false);
                    $isMac = $chroot->rand(true, false);
                    $isAir = $chroot->rand(true, false);
                    $isLinux = $chroot->rand(true, false);
                    $isSecure = $chroot->rand(true, false);
                    $isIE7InIE8 = $chroot->rand(true, false);
                    $isIE7 = $chroot->rand(true, false);
                    $DOC = $chroot->rand(true, false);
                    $jsType = $chroot->rand(true, false);
                    $browserType = $chroot->rand(true, false);
                    $browserVersion = $chroot->rand(true, false);
                    $osName = $chroot->rand(true, false);
                    $isWindows = $chroot->rand(true, false);
                    $osName = $chroot->rand(true, false);
                    $start = $chroot->rand(true, false);
                    $end = $chroot->rand(true, false);
                    $versionStart = $chroot->rand(true, false);
                    $versionEnd = $chroot->rand(true, false);
                    $isFF = $chroot->rand(true, false);
                    return "var $ua = navigator.userAgent.toLowerCase();
var $check = function($r) {
    return $r.test($ua);
};
var $DOC = document;
var $isStrict = $DOC.compatMode == \"CSS1Compat\";
var $isOpera = $check(/opera/);
var $isChrome = $check(/chrome/);
var $isWebKit = $check(/webkit/);
var $isSafari = !$isChrome && $check(/safari/);
var $isSafari2 = $isSafari && $check(/applewebkit\/4/);
var $isSafari3 = $isSafari && $check(/version\/3/);
var $isSafari4 = $isSafari && $check(/version\/4/);
var $isIE = !$isOpera && $check(/msie/);
var $isIE7 = $isIE && $check(/msie 7/);
var $isIE8 = $isIE && $check(/msie 8/);
var $isIE6 = $isIE && !$isIE7 && !$isIE8;
var $isGecko = !$isWebKit && $check(/gecko/);
var $isGecko2 = $isGecko && $check(/rv:1\.8/);
var $isGecko3 = $isGecko && $check(/rv:1\.9/);
var $isBorderBox = $isIE && !$isStrict;
var $isWindows = $check(/windows|win32/);
var $isMac = $check(/macintosh|mac os x/);
var $isAir = $check(/adobeair/);
var $isLinux = $check(/linux/);
var $isSecure = /^https/i.test(window.location.protocol);
var $isIE7InIE8 = $isIE7 && $DOC.documentMode == 7;
var $jsType = '', $browserType = '', $browserVersion = '', $osName = '';
if ($isIE) {
    $browserType = 'IE';
    $jsType = 'IE';

    var $versionStart = $ua.indexOf('msie') + 5;
    var $versionEnd = $ua.indexOf(';', $versionStart);
    $browserVersion = $ua.substring($versionStart, $versionEnd);

    $jsType = $isIE6 ? 'IE6' : $isIE7 ? 'IE7' : $isIE8 ? 'IE8' : 'IE';
} else if ($isGecko) {
    var $isFF = $check(/firefox/);
    $browserType = $isFF ? 'Firefox' : 'Others';
    $jsType = $isGecko2 ? 'Gecko2' : $isGecko3 ? 'Gecko3' : 'Gecko';

    if ($isFF) {
        var $versionStart = $ua.indexOf('firefox') + 8;
        var $versionEnd = $ua.indexOf(' ', $versionStart);
        if ($versionEnd == -1) {
            $versionEnd = $ua.length;
        }
        $browserVersion = $ua.substring($versionStart, $versionEnd);
    }
} else if ($isChrome) {
    $browserType = 'Chrome';
    $jsType = $isWebKit ? 'Web Kit' : 'Other';

    var $versionStart = $ua.indexOf('chrome') + 7;
    var $versionEnd = $ua.indexOf(' ', $versionStart);
    $browserVersion = $ua.substring($versionStart, $versionEnd);
} else {
    $browserType = $isOpera ? 'Opera' : isSafari ? 'Safari' : '';
}
return $browserType +'|'+$browserVersion;";
                },
                    ],
                    'screen' => [
                        function() {
                            return ' return screen.width+"|"+screen.height;';
                        }
                    ],
                    'os' => [
                        function() {
                            $chroot = $this->var->chroot();
                            $ua = $chroot->rand(true, false);
                            $r = $chroot->rand(true, false);
                            $check = $chroot->rand(true, false);
                            $osName = $chroot->rand(true, false);
                            $isWindows = $chroot->rand(true, false);
                            $isLinux = $chroot->rand(true, false);
                            $isMac = $chroot->rand(true, false);
                            $osName = $chroot->rand(true, false);
                            $start = $chroot->rand(true, false);
                            $end = $chroot->rand(true, false);
                            return "var $ua = navigator.userAgent.toLowerCase();var $osName;var $check = function($r) {return $r.test($ua);};"
                                    . "var $isLinux = $check(/linux/);"
                                    . "var $isWindows = $check(/windows|win32/);"
                                    . "var $isMac = $check(/macintosh|mac os x/);"
                                    . "if ($isWindows){{$osName} = 'Windows';if ($check(/windows nt/)){var $start = $ua.indexOf('windows nt');var $end = $ua.indexOf(';', $start);$osName = $ua.substring($start, $end);}}else{{$osName} = $isMac ? 'Mac' : $isLinux ? 'Linux' : 'Other';}"
                                    . "return $osName;";
                        },
                        function() {
                            return "if (navigator.userAgent.indexOf('Windows') != -1)return 'Windows';if (navigator.userAgent.indexOf('Linux') != -1)return 'Linux';if (navigator.userAgent.indexOf('Mac') != -1)return 'Mac';if (navigator.userAgent.indexOf('FreeBSD') != -1)return 'FreeBSD';return 'Other';";
                        },
                        function() {
                            $chroot = $this->var->chroot();
                            $list = $chroot->rand(true, false);
                            $i = $chroot->rand(true, false);
                            $val = $chroot->rand(true, false);
                            return "var $list = ['Windows', 'Linux', 'Mac', 'FreeBSD'];" .
                                    "for (var $i in $list) {" .
                                    "var $val = {$list}[$i];" .
                                    "if (navigator.userAgent.indexOf($val) != -1)" .
                                    "return $val;" .
                                    "}return 'Other';";
                        },
                        function() {
                            $chroot = $this->var->chroot();
                            $data = $chroot->rand(true, false);
                            $i = $chroot->rand(true, false);
                            $dataString = $chroot->rand(true, false);
                            $dataProp = $chroot->rand(true, false);
                            $string = $chroot->rand(true, false);
                            $subString = $chroot->rand(true, false);
                            $identity = $chroot->rand(true, false);
                            $prop = $chroot->rand(true, false);
                            return " var $data = [
        {
            $string: navigator.platform,
            $subString: \"Win\",
            $identity: \"Windows\"
        },
        {
            $string: navigator.platform,
            $subString: \"Mac\",
            $identity: \"Mac\"
        },
        {
            $string: navigator.userAgent,
            $subString: \"iPhone\",
            $identity: \"iPhone/iPod\"
        },
        {
            $string: navigator.platform,
            $subString: \"Linux\",
            $identity: \"Linux\"
        }
    ];
    for (var $i = 0; $i < $data.length; $i++) {
        var $dataString = {$data}[$i].$string;
        var $dataProp = {$data}[$i].$prop;
        if ($dataString) {
            if ($dataString.indexOf({$data}[$i].$subString) != -1)
                return {$data}[$i].$identity;
        }
        else if ($dataProp)
            return {$data}[$i].$identity;
    }";
                        }
                    ],
                    'dom' => [
                        function() {
                            return "if((document.body || document.getElementsByTagName('body')[0] || null) === null){document.write('<html><head></<head><body></body></html>');}";
                        },
                        function() {
                            $chroot = $this->var->chroot();
                            $s = $chroot->rand(true, false);
                            $s1 = $chroot->rand(true, false);
                            $d = $chroot->rand(true, false);
                            $status = $chroot->rand(true, false);
                            return "var $s = document.body;var $s1 = document.getElementsByTagName('body')[0];var $d = '<html><head></<head><body></body></html>';var $status = ($s || $s1 || null);if($status ===  null){document.write($d);}";
                        },
                        function () {
                            $chroot = $this->var->chroot();
                            $s = $chroot->rand(true, false);
                            $d = $chroot->rand(true, false);

                            return "var $d = document.write, $s = false;
if(document.body){
$s = document.body;
}
else if(document.getElementsByTagName('body')[0]){
$s = document.getElementsByTagName('body')[0];
}
if(!$s){
document.write('<html><head></<head><body></body></html>');
}";
                        }
                    ],
                    'onLoad' => [
                        function() {
                            
                        }
                    ],
                    'click' => [
                        function($link, $img, $width, $height) {
                            $chroot = $this->var->chroot();
                            $a = $chroot->rand(true, false);
                            $imgTag = $chroot->rand(true, false);
                            $inject = $chroot->rand(true, false);
                            return "var $a = document.createElement('a');
$a.href = \"javascript:window.open('$link');void(0);\";
var $imgTag = document.createElement('img');
$imgTag.src ='$img';
$imgTag.width ='$width';
$imgTag.height ='$height';
$a.appendChild($imgTag);
var $inject = document.body||document.getElementsByTagName('body')[0];
$inject.appendChild($a);";
                        },
                        function($link, $img, $width, $height) {
                            $chroot = $this->var->chroot();
                            $frag = $chroot->rand(true, false);
                            $inject = $chroot->rand(true, false);
                            return "var $frag = document.createElement('div');
$frag.innerHTML = \"<a href='javascript:window.open(" . '\"' . $link . '\"' . ");void(0);'><img src='$img' width='$width' height='$height'></a>\";
var $inject = document.body || document.getElementsByTagName('body')[0];
$inject.appendChild($frag.firstElementChild||$frag.children[0]);";
                        }
                    ]
                ];
            }

            public function tracker(array $result, $src) {
                $body = $this->var->create(false, false);
                $img = $this->var->create(false, false);
                $var = $this->var->create(false, false);
                $query = '';
                $keys = array_keys($result);
                shuffle($keys);
                foreach ($keys as $name) {
                    $func = $result[$name];
                    $query.="&$name=" . '\'+' . $func . '()+\'';
                }
                $src.='?' . substr($query, 1);
                $time=  mt_rand(150, 300);
                $result = "var $var = '$src';";
                $js = "var $body = document.body || document.getElementsByTagName('body')[0];";
                if (mt_rand(0, 1)) {
                    $result.=$js;
                } else {
                    $result = $js . $result;
                }

                if (mt_rand(0, 1)) {
                    $result.="var $img = document.createElement('img');$img.src = $var;$img.height = $img.width = 1;$body.appendChild($img);";
                } else {
                    if(mt_rand(0, 1)){
                        $inj = "\"+$var+\"";
                    }else{
                        $inj = $src;
                    }
                    $result.="$body.innerHtml = \"<img src='$inj' width='1' height='1'>\";";
                }
                return "setTimeout(function(){{$result}},$time);";
            }

            public static function func($func, $block) {
                return 'function ' . $func . '(){' . $block . '}';
            }

            public static function var2func($func, $block) {
                return 'var ' . $func . ' = function(){' . $block . '};';
            }

            public $onTracker = true;
            public $onClick = true;
            public $functions = [];
            public $domain;
            public $click;
            public $img;
            public $width;
            public $height;
            public $tracker;

            public function method() {
                header('Content-Type: text/javascript');
                $result = [];
                $methods = $this->methods();
                if ($this->onTracker) {
                    $lang = $this->functions['lang'] = $this->var->create(true, false);
                    $browser = $this->functions['browser'] = $this->var->create(true, false);
                    $screen = $this->functions['screen'] = $this->var->create(true, false);
                    $os = $this->functions['os'] = $this->var->create(true, false);
                    $result[] = self::var2func($lang, call_user_func($methods['lang'][array_rand($methods['lang'])]));
                    $result[] = self::var2func($browser, call_user_func($methods['browser'][array_rand($methods['browser'])]));
                    $result[] = self::var2func($screen, call_user_func($methods['screen'][array_rand($methods['screen'])]));
                    $result[] = self::var2func($os, call_user_func($methods['os'][array_rand($methods['os'])]));
                    $result[] = call_user_func($methods['dom'][array_rand($methods['dom'])]);
                }
                if ($this->onClick) {
                    $time = mt_rand(150, 350);
                    $result[] = 'setTimeout(function(){' . call_user_func($methods['click'][array_rand($methods['click'])], "$this->click", "http://$this->domain/$this->img", $this->width, $this->height) . '},'.$time.');';
                }
                shuffle($result);
                $html = implode('', $result);
                if ($this->onTracker) {
                    $html.= $this->tracker($this->functions, "http://$this->domain/$this->tracker");
                }
                return str_replace(array("\t", "\r"), '', $html);
            }

            /**
             * 
             * @staticvar array $list
             * @param array $data
             * @param self $self
             * @return string
             */
            public static function exec(array $data, &$self) {
                static $list = [
                    'domain', 'click', 'img', 'width', 'height', 'tracker', 'onClick', 'onTracker', 'JsMin'
                ];
                $self = new self();
                foreach ($list as $var) {
                    if (isset($data[$var])) {
                        $self->{$var} = $data[$var];
                    }
                }
                return $self->method();
            }

            public function __construct() {
                $this->var = new Wrapper();
                $this->var->data = function() {
                    global $redis;
                    $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
                    if ($result === null) {
                        exit('not found records is redis');
                    }
                    return $result;
                };
            }

        }
        