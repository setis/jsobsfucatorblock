<?php

class gVar_v1 {

    /**
     *
     * @var Array
     */
    public static $list;

    /**
     *
     * @var int
     */
    public static $size;

    /**
     *
     * @var array|int
     */
    public $value;

    public function __construct() {
        self::createList();
        $this->value[] = -1;
    }

    public static function createList() {
        if (self::$list === null) {
            $list = range('a', 'z');
            array_map(function($value)use($list) {
                $list[] = $value;
            }, range('A', 'Z'));
            self::$size = count($list) - 1;
            self::$list = $list;
        }
    }

    public function reset() {
        $this->value[] = -1;
    }

    public function run() {
        $size = count($this->value);
        $value = [];
        $this->value[$size - 1] ++;
        while (--$size >= 0) {
            $s = $this->value[$size];
            if (self::$size < $s) {
                $this->value[$size] = 0;
                if ($size === 0) {
                    $this->value[count($this->value)] = -1;
                } else {
                    $this->value[$size - 1] ++;
                }
            }
            $value[$size] = $this->value[$size];
        }
        return implode('', array_map(function($v) {
                    return self::$list[$v];
                }, $value));
    }

    public function __invoke() {
        return $this->run();
    }

}

class gVar_v2 {

    /**
     *
     * @var Array
     */
    public static $list;

    /**
     *
     * @var int
     */
    public $len = 1;

    /**
     *
     * @var Wrapper 
     */
    public $Wrapper;

    /**
     *
     * @var int
     */
    public static $size;

    /**
     *
     * @var boolean
     */
    public $rand = false;

    /**
     *
     * @var array|int
     */
    public $value;

    /**
     *
     * @var int
     */
    public $count = 5;
    public $counter = 0;

    /**
     *
     * @var array
     */
    public $values = [];

    public function __construct($wrapper = null, array $config = null) {
        if ($wrapper instanceof Wrapper) {
            $this->Wrapper = &$wrapper;
        } else if (is_array($wrapper)) {
            $this->Wrapper = new Wrapper();
            $this->Wrapper->generate = $this;
            $this->Wrapper->restore($wrapper);
        } else {
            $this->Wrapper = new Wrapper();
            $this->Wrapper->generate = $this;
        }
        self::createList();
        if ($config === null) {
            $config = [
                'rand' => false,
                'count' => 5
            ];
        }
        $this->setConfig($config);
    }

    public static function createList() {
        if (self::$list === null) {
            $list = range('a', 'z');
            array_map(function($value)use($list) {
                $list[] = $value;
            }, range('A', 'Z'));
            self::$size = count($list) - 1;
            self::$list = $list;
        }
    }

    /**
     * 
     * @return array
     */
    public function save() {
        return [
            $this->value,
            $this->rand,
            $this->count,
            $this->counter
        ];
    }

    public function reset() {
        if ($this->rand === true) {
            $this->value = 1;
        } else {
            $this->value[] = -1;
        }
        $this->count = 5;
        $this->counter = 0;
    }

    /**
     * 
     * @param array $cfg
     */
    public function restore(array $cfg) {
        list ($value, $rand, $count, $counter) = $cfg;
        $cfg = [];
        if ($rand !== null) {
            $cfg['rand'] = $rand;
        }
        if ($count !== null) {
            $cfg['count'] = $count;
        }
        if ($counter !== null) {
            $cfg['counter'] = $counter;
        }
        $this->setConfig($cfg);
        $this->value = $value;
    }

    public function setConfig(array $config) {
        if (isset($config['rand'])) {
            $this->rand = $config['rand'];
        }
        if ($this->rand === true) {
            $this->value = 1;
        } else {
            $this->value[] = -1;
        }
        if (isset($config['size'])) {
            $this->count = $config['size'];
            $i = 0;
            $s = self::$size + 1;
            while ($i <= $this->count) {
                $this->values[$i] = pow($s, ++$i);
            }
        }
    }

    public function generate() {
        if ($this->rand === true) {
            $value = '';
            $i = 0;
            if ($this->counter > $this->values[$this->value]) {
                $this->value++;
            }
            while ($i <= $this->value) {
                $value.=$this->list[mt_rand(0, self::$size)];
                $i++;
            }
            return $value;
        } else {
            $size = count($this->value);
            $value = [];
            $i = 0;
            $this->value[$size - 1] ++;
            while (--$size >= 0) {
                $s = $this->value[$size];
                if (self::$size < $s) {
                    $this->value[$size] = 0;
                    if ($size === 0) {
                        $this->value[count($this->value)] = -1;
                    } else {
                        $this->value[$size - 1] ++;
                    }
                }
                $value[$size] = $this->value[$size];
            }
            return implode('', array_map(function($v) {
                        return self::$list[$v];
                    }, $value));
        }
    }

    public function run() {
        $data = null;
        while (true) {
            $data = $this->generate();
            if (!in_array($data, $this->Wrapper->uniq) && !in_array($data, $this->Wrapper->list) && ( empty($this->root) || $this->Wrapper->chroot === false || $this->Wrapper->global === false || !in_array($data, $this->Wrapper->root->uniq))) {
                if ($this->rand === true) {
                    $this->counter++;
                }
                return $this->Wrapper->list[] = $data;
            }
        }
    }

    public function __invoke() {
        return $this->run();
    }

}
