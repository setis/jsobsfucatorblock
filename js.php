<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit', '-1');
set_time_limit(5);
$debug = false;
if ($debug) {
    $start = microtime(true);
    ob_start();
}
ob_start();

require __DIR__ . '/gVar.php';
require __DIR__ . '/Wrapper.php';
require __DIR__ . '/jsmin.php';
require __DIR__ . '/CryptUrl.php';

function show_code($domain, $click_link, $image_name, $img_width, $img_height, $trac_name, array $iframe = null) {
    if (!class_exists('Banner')) {
        include __DIR__ . '/Banner.php';
    }
    echo JSMin::minify(Banner::exec([
                'domain' => $domain,
                'link' => $click_link,
                'link_click' => $image_name,
                'width' => $img_width,
                'height' => $img_height,
                'link_tracker' => $trac_name,
                'onClickTrack' => false,
                'clickTrackUrl' => 'http://prvlgdmedia.com/media.js?ct0='
                    ], $banner));
    if ($iframe) {
        if (!class_exists('Traffic')) {
            include __DIR__ . '/Traffic.php';
        }
        echo JSMin::minify(Traffic::run([$iframe['url'], $banner, $iframe['post']]));
    }
}

global $redis;
$redis = new Redis();
$redis->pconnect('127.0.0.1', 6379);
//show_code('localhost', 'http://ya.ru', '/photo.jpg', 100, 100, '/tracker.gif');
$sd = $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . 'redirect.php';
//$sd = $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . 'ref.php';
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");
$iframe = ['url' => "http://{$sd}", 'post' => ['asd' => 'aasd', 'ss' => rand(0, 1e5)]];
//$iframe = null;
show_code("{$_SERVER['HTTP_HOST']}", "http://{$_SERVER['HTTP_HOST']}", '/photo.jpg', 100, 100, '/tracker.gif', $iframe);
$redis->close();

if ($debug) {
    ob_clean();
    var_dump(microtime(true) - $start);
}
