<?php

class CryptUrl {

    /**
     *
     * @var Wrapper;
     */
    public $Wrapper;
    public $link_int, $link_size;
    public $decode;
    public $handler;

    public function __construct($wrapper = null) {
        if ($wrapper instanceof Wrapper) {
            $this->Wrapper = $wrapper;
        } else if (is_array($wrapper)) {
            $this->Wrapper = new Wrapper();
            $this->Wrapper->data = function() {
                global $redis;
                $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
                if ($result === null) {
                    exit('not found records is redis');
                }
                return $result;
            };
            $this->Wrapper->restore($wrapper);
        } else {
            $this->Wrapper = new Wrapper();
            $this->Wrapper->data = function() {
                global $redis;
                $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
                if ($result === null) {
                    exit('not found records is redis');
                }
                return $result;
            };
        }
    }

    /**
     * 
     * @param string $link
     * @param Wrapper|Array $wrapper
     * @param $this|static|self $self
     * @return string
     */
    public static function run($link, $wrapper = null, &$self = null) {
        $self = new self($wrapper);
        return $self->crypt($link);
    }

    public function crypt($link) {
        list($this->link_int, $this->link_size) = self::encode($link);
        $this->decode = $this->Wrapper->rand(true, false);
        $this->handler = $this->Wrapper->rand(true, false);
        $result[] = $this->decode();
//        foreach ($this->callbacks() as $callback) {
//            $result[] = $callback;
//        }
        $result[] = $this->handler();
        shuffle($result);
        return implode('', $result);
    }
    /**
     * 
     * @return \CryptUrl
     */
    public function clear(){
        $this->link_int = null;
        $this->link_size = null;
        $this->handler = null;
        $this->decode = null;
        return $this;
    }

    public static function encode($str) {
        $result = false;
        for ($a = 0; $a < strlen($str); $a++) {
            $value = ord(substr($str, $a, 1));
            $result[2][$a]=[$value,substr($str, $a, 1)];
            
//            list(, $value) = unpack('N', mb_convert_encoding(substr($str, $a, 1), 'UCS-4BE', 'UTF-8'));
            $result[0][] = $value;
            $result[1][] = strlen($value);
        }
//        echo '/* '.var_export($result[2],true).' */';
        return $result;
    }

    public function decode() {
        $chroot = $this->Wrapper->chroot();
        $string = $chroot->rand(true, false);
        $start = $chroot->rand(true, false);
        $end = $chroot->rand(true, false);
        return "function {$this->decode}($string, $start, $end) {"
                . "return String.fromCharCode($string.toString().substr($start, $end));"
                . "}";
    }

    public $callbacksUse;
    public $callbacksCell;

    public function callbacks() {
        $this->callbacksCell = $this->callbacksUse = [];

        $randCell = mt_rand(7, 20);
        $randUse = rand(0, (count($this->link_int) - 1));

        $size = count($this->link_size);
        $listUniq = [];
        while (count($this->callbacksUse) !== $randUse) {
            $len = rand(0, $size - 1);
            if (!in_array($len, $listUniq)) {
                $this->callbacksUse[$this->Wrapper->rand(true, FALSE)] = $len;
                $listUniq[] = $len;
            }
        }
        $this->callbacksUse = array_combine(array_values($this->callbacksUse), array_keys($this->callbacksUse));
        $size2 = $size * 5;
        while (count($this->callbacksCell) !== $randCell) {
            $len = rand(0, $size2);
            if (!in_array($len, $listUniq)) {
                $this->callbacksCell[$this->Wrapper->rand(true, FALSE)] = $len;
                $listUniq[] = $len;
            }
        }
        $this->callbacksCell = array_combine(array_values($this->callbacksCell), array_keys($this->callbacksCell));
        $result = [];
        $print = function($name, $n) {
            return "function {$name}(){"
                    . "return $n;"
                    . "}";
        };
        foreach ($this->callbacksUse as $i => $name) {
            $d = mt_rand(0, 1);
            if (true) {
                $array = $this->callGenerate($this->link_int[$i], $s);
                $this->link_size[$i] = strlen($s);
                $this->link_int[$i] = $s;
                $result[] = $this->callPrint($array, $name);
            } else {
                $result[] = call_user_func($print, $name, $s);
                $this->link_size[$i] = strlen($s);
                $this->link_int[$i] = $s;
            }
        }
        foreach ($this->callbacksCell as $i => $name) {
            $d = rand(0, 1);
            if (false) {
                $array = $this->callGenerate(rand(1, 254), $s);
                $this->link_size[$i] = strlen($s);
                $result[] = $this->callPrint($array, $name);
            } else {
                $result[] = $print($name, rand(1, 254));
            }
        }
        return $result;
    }

    const division = 0, multiply = 1, subtract = 2, add = 3;

    public static function calculator($method, $n1, $n2) {
        switch ($method) {
            case '/':
            case self::division:
                return $n1 / $n2;
            case '*':
            case self::multiply:
                return $n1 * $n2;
            case '-':
            case self::subtract:
                return $n1 - $n2;
            case '+':
            case self::add:
                return $n1 + $n2;
        }
    }

    public static $rands = [
        self::division => [1, 9],
        self::multiply => [2, 10],
        self::subtract => [0, 255],
        self::add => [0, 255]
    ];
    public static $list = [
        '/', '*', '-', '+'
    ];
    public $optimize = true;

    public function callGenerate($n, &$s) {
        $count = mt_rand(1, 10);
        $result = [];
        while (true) {
            $operation = mt_rand(0, 3);
            list($min, $max) = self::$rands[$operation];
            $n1 = $n;
            $n2 = rand($min, $max);
            $eq = self::calculator($operation, $n1, $n2);
            if (count($result) === $count) {
                if ($eq >= 0 && $eq <= 255 && !is_float($eq)) {
                    $s = $eq;
                    return $result;
                } else {
                    if ($this->optimize) {
                        $s = rand(1, 254);
                        $diff = $s - abs($eq);
                        if ($eq > 0 && $diff > 0) {
                            $result[] = [self::subtract, $diff];
                        } else {
                            $result[] = [self::add, $diff];
                        }
                        return $result;
                    } else {
                        $result = [];
                    }
                }
            } else {
                $result[] = [$operation, $n2];
            }
        }
    }

    public static function size(array $array) {
        return strlen(current(end($array))[1]);
    }

    public function callPrint($array, $name = null) {
        if ($name === null) {
            $name = $this->Wrapper->rand(true, false);
        }
        $var = $result = $this->Wrapper->rand(false, false);
        foreach ($array as $i => $value) {
            list($operation, $n2) = $value;
            $result = '(' . $result . self::$list[$operation] . $n2 . ')';
        }
        return "function $name($var){"
                . "return $result;"
                . "}";
    }

    public function vars() {
        
    }

    public function handler() {
//        $arr = $this->callbacksCell + $this->callbacksUse;
//        $callbacksData = json_encode($arr);
        $listData = json_encode($this->link_size);
        $varsData = json_encode([]);
        $cryptData = implode('', $this->link_int);
        $chroot = $this->Wrapper->chroot();
        $chroot->list[] = $this->decode;
        $chroot->lock($this->decode);

        $callbacks = $chroot->rand(true, false);
        $list = $chroot->rand(true, false);
        $vars = $chroot->rand(true, false);
        $crypt = $chroot->rand(true, false);
        $len = $chroot->rand(true, false);
        $return = $chroot->rand(true, false);

        $i = $chroot->rand(true, false);
        $size = $chroot->rand(true, false);
        $value = $chroot->rand(true, false);
        $call = $chroot->rand(true, false);
        $var = $chroot->rand(true, false);

//        foreach (['list', 'vars', 'len', 'crypt', 'call', 'value', 'i', 'size', 'callbacks'] as $v) {
//            ${$v} = $v;
//        }
        $callbacksData = '[]';

        $result[] = "$list = $listData";
        $result[] = "$vars = $varsData";
        $result[] = "$crypt = \"$cryptData\"";
        $result[] = "$callbacks = $callbacksData";
        $result[] = "$len = 0";
        $result[] = "$return = []";
        shuffle($result);
        $result = implode(',', $result);

        $result2[] = "$size = {$list}[$i]";
        $result2[] = "$value = Number($crypt.substr($len, $size))";
        $result2[] = "$call = {$callbacks}[$len]";
        $result2[] = "$var = {$vars}[$len]";
        shuffle($result2);
        $result2 = implode(',', $result2);
        return "function $this->handler(){"
                . "var $result;"
                . "for(var $i in $list){"
                . "var $result2;"
                . "if(String($value).length !== $size){"
//                . "console.info($len,$size,$crypt,$value,'Number(\"$cryptData\".substr('+$len+', '+$size+'))');"
                . "$value = '';"
                . "for(var d = $len;d<($len+$size);d++){"
                . "$value+=Number({$crypt}[d]);"
                . "}"
                . "$value =Number($value);"
//                . "console.info($len,$size,$crypt,$value,'Number(\"$cryptData\").substr('+$len+', '+$size+'))');"
                . "}"
                . "if ($call !== undefined) {"
                . "if($var) {"
                . "this[$var] =  String.fromCharCode((typeof $call === 'function') ? $call($value) : this[$call]($value));"
                . "}else{ "
                . "{$return}.push(String.fromCharCode((typeof $call === 'function') ? $call($value) : this[$call]($value)));"
                . "}}else{"
                . "($var) ? this[$var] = String.fromCharCode($value) : {$return}.push(String.fromCharCode($value));"
                . "}"
//                . "console.info($len,$size,$crypt,$value,String($value).length,'Number(\"$cryptData\".substr('+$len+', '+$size+'))',($call !== undefined),$return);"
                . "$len +=Number($size);"
                . "}"
                . "return $return.join('');"
                . "}";
    }


}
