<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit', '-1');
set_time_limit(10);
ob_start();
global $redis;
$redis = new Redis();
$redis->pconnect('127.0.0.1', 6379);
require __DIR__ . '/CryptUrl.php';
require __DIR__ . '/Base.php';
require __DIR__ . '/Wrapper.php';
require __DIR__ . '/jsmin.php';
require __DIR__ . '/Traffic.php';

$sd = $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . 'ref.php';
$iframe = ['url' => "http://{$sd}", 'post' => ['asd' => 'aasd', 'ss' => rand(0, 1e5)]];
echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body><script>';
echo JSMin::minify(Traffic::run(['link'=>$iframe['url'], 'params'=>$iframe['post'],'onCrypt'=>true,'self'=>true]));
echo '</script></body></html>';