<?php

require __DIR__ . '/Wrapper.php';
require __DIR__ . '/jsmin.php';
require __DIR__ . '/CryptUrl.php';

function show_code($domain, $click_link, $image_name, $img_width, $img_height, $trac_name, array $iframe = null) {
    if (!class_exists('Banner')) {
        include __DIR__ . '/Banner.php';
    }

    global $redis;

    if ($_code = $redis->get('tds:cache:' . SITE)) {
        list($code, $vars) = json_decode($_code, true);
        header('Content-Type: text/javascript');
    } else {
        $code = JSMin::minify(Banner::exec([
                            'domain' => '__domain__',                                                                                                                                           
                            'click' => '__clicklink__',                                                                                                                                         
                            'img' => '__image_name__',                                                                                                                                          
                            'width' => '__img_width__',                                                                                                                                         
                            'height' => '__img_height__',                                                                                                                                       
                            'tracker' => '__trac_name__'                                                                                                                                        
                                ], $banner));                                                                                                                                                   
        $vars = $banner->var->save();
        $redis->setex('tds:cache:' . SITE, 86400, json_encode([$code,$vars]));
        
    }

    $code = str_replace('__domain__', $domain, $code);
    $code = str_replace('__clicklink__', $click_link, $code);
    $code = str_replace('__image_name__', $image_name, $code);
    $code = str_replace('__img_width__', $img_width, $code);
    $code = str_replace('__img_height__', $img_height, $code);
    $code = str_replace('__trac_name__', $trac_name, $code);

    echo $code;

    if ($iframe) {
        if (!class_exists('Traffic')) {
            include __DIR__ . '/Traffic.php';
        }
        echo JSMin::minify(Traffic::run([$iframe['url'], $vars, $iframe['post']]));
    }
}

/**
 * 
 * @param string $Found
 * @param string $notFound
 */
function show_proc($Found, $notFound) {
    if (!class_exists('Redirect')) {
        include __DIR__ . '/Redirect.php';
    }
    $_code = JSMin::minify(Redirect::run($notFound, $Found));

    echo '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body><script>';
    echo $_code;
    echo '</script></body></html>';
}
