<?php

class Wrapper implements Countable {

    public $uniq = array();
    public $list = array();
    public $data;
    public $prefix;
    public $chroot = false;
    public $global = false;

    /**
     *
     * @var $this
     */
    public $root;

    const
            type_create = 0,
            type_use = 1;

    public $list_exceptions = array('continue', 'function', 'var', 'break', 'else', 'this');

    public function iself($self) {
        $this->self = $self;
        return $this;
    }

    public function generate() {
        $data = null;
        while (true) {
            $data = call_user_func($this->data);
                        if (!in_array($data, $this->list_exceptions) && !in_array($data, $this->uniq) && !in_array($data, $this->list) && ( empty($this->root) ||$this->chroot === false || $this->global === false || !in_array($data, $this->root->uniq))) {
                return $this->list[] = $data;
            }
        }
    }

    public function check($data) {
        return (!in_array($data, $this->uniq) && !in_array($data, $this->list));
    }

    public function reset() {
        $this->uniq = array();
        $this->list = array();
    }

    /**
     * 
     * @return $this
     */
    public function chroot() {
        $chroot = clone $this;
        $chroot->reset();
        $chroot->chroot = true;
        $chroot->root = $this;
        return $chroot;
    }

    public function __destruct() {
        if ($this->chroot === true) {
            $this->root->list = array_merge($this->root->list, $this->toArray());
        }
    }

    public function rand($lock = false, $wrapper = true, $i = 0) {
        while (true) {
            $d = mt_rand(0, 1);
            $save = $this->save();
            try {
                if ($d !== self::type_create) {
                    return $this->select($lock);
                } else {
                    return $this->create($lock, $wrapper);
                }
            } catch (Exception $e) {
                $this->restore($save);
            }
        }
    }

    public function lock() {
        $array = func_get_args();
        switch (func_num_args()) {
            case 0:
                throw new \InvalidArgumentException('not lock');
            case 1:
                if (is_array($array[0])) {
                    $array = $array[0];
                }
                break;
        }
        foreach ($array as $var) {
            $flag = 0;
            if (false === ($id = array_search($var, $this->list))) {
                $flag += 1;
            }
            if (in_array($var, $this->uniq)) {
                $flag += 2;
            }
            switch ($flag) {
                case 0:
                    $this->uniq[] = $var;
                    unset($this->list[$id]);
                    break;
                case 1:
                    //нет таких в регистре данных
                    throw new Exception('there is no such register');
                case 2:
                    //уже заблокирована
                    throw new Exception('already locked');
                case 3:
                    throw new Exception('there is no such register && already locked');
            }
        }
//        var_dump($this->save(), func_get_args(), __METHOD__);
    }

    public function block($var, $lock = true) {
        if ($var === null || $var === '') {
            //пустые данные
            throw new Exception('empty data');
        }
        if ($lock === true) {
            if (false === ($id = array_search($var, $this->list))) {
                //нет такой в регистре переменной
                throw new Exception('there is no such register');
            }
            if (in_array($var, $this->uniq)) {
                //уже заблокирована
                throw new Exception('already locked');
            }
            $this->uniq[] = $var;
            unset($this->list[$id]);
//            var_dump($this->save(), func_get_args(), __METHOD__);
        } else {
            $id = array_search($var, $this->uniq);
            if ($id) {
                $var = $this->uniq[$id];
                unset($this->uniq[$id]);
                $this->list[] = $var;
                shuffle($this->list);
            }
        }
    }

    public function unlock() {
        if (func_num_args() > 1) {
            $array = func_get_args();
        } else {
            $array[] = func_get_arg(1);
        }
        foreach ($array as $var) {
            $id = array_search($var, $this->uniq);
            if ($id) {
                $var = $this->uniq[$id];
                unset($this->uniq[$id]);
                $this->list[] = $var;
            }
        }
        shuffle($this->list);
    }

    public function save() {
        return array($this->uniq, $this->list);
    }

    public function restore($array) {
        list($this->uniq, $this->list) = $array;
    }

    public function create($lock = false, $prefix = true) {
        $var = $this->generate();
        if ($lock === true) {
            $this->block($var, $lock);
        }
        return ($prefix === true) ? $this->prefix . $var : $var;
    }

    public function select($lock = false) {
        if (count($this->list) === 0) {
            throw new Exception('list empty');
        }
        $id = array_rand($this->list);
        $var = $this->list[$id];
        if ($lock === true) {
            $this->block($var, $lock);
        }
        return $var;
    }

    public function count() {
        return count($this->list) + count($this->uniq);
    }

    public function toArray() {
        $result = $this->list;
        foreach ($this->uniq as $var) {
            $result[] = $var;
        }
        return $result;
    }

}

